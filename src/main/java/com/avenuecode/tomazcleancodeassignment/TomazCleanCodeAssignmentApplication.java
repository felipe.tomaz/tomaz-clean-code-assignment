package com.avenuecode.tomazcleancodeassignment;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TomazCleanCodeAssignmentApplication {

	public static void main(String[] args) {
		SpringApplication.run(TomazCleanCodeAssignmentApplication.class, args);
	}

}
