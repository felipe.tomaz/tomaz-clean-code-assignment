package com.avenuecode.tomazcleancodeassignment.user.infra.authentication;

import com.avenuecode.tomazcleancodeassignment.user.application.interfaces.AuthenticationStrategy;
import com.avenuecode.tomazcleancodeassignment.user.domain.valueobjects.Email;

public class GoogleAuthenticator implements AuthenticationStrategy {
    @Override
    public UserData authenticate(Email email, String password) {
        return new UserData("", "google token");
    }
}
