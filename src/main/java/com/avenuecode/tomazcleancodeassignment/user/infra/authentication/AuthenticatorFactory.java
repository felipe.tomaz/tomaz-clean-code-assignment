package com.avenuecode.tomazcleancodeassignment.user.infra.authentication;

import com.avenuecode.tomazcleancodeassignment.user.application.interfaces.AuthenticationStrategy;
import com.avenuecode.tomazcleancodeassignment.user.errors.UnsupportedEmailDomainException;

public class AuthenticatorFactory {
    public static AuthenticationStrategy createAuthenticator(String email) throws UnsupportedEmailDomainException {

        if(email.endsWith("@yahoo.com")) {
            return new YahooAuthenticator();
        }

        if(email.endsWith("@google.com")) {
            return new GoogleAuthenticator();
        }

        if (email.endsWith("@outlook.com") || email.endsWith("@hotmail.com")) {
            return new OutlookAuthenticator();
        }

        throw new UnsupportedEmailDomainException(email);
    }
}

