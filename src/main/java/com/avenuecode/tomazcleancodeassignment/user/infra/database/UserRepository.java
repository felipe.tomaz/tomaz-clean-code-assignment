package com.avenuecode.tomazcleancodeassignment.user.infra.database;

import org.springframework.data.repository.CrudRepository;

public interface UserRepository extends CrudRepository<UserModel, String> {

}
