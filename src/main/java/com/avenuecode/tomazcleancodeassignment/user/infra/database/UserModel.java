package com.avenuecode.tomazcleancodeassignment.user.infra.database;

import com.avenuecode.tomazcleancodeassignment.user.domain.User;
import lombok.Getter;

import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class UserModel {
    @Getter @Id private String email;
    @Getter private String name;
    @Getter private String avatarURL;

    public UserModel(String email, String name, String avatarURL) {
        this.email = email;
        this.name = name;
        this.avatarURL = avatarURL;
    }

    public UserModel(){}

    public static UserModel parseToModel(User user) {
        String name = user.getName().getValue();
        String email = user.getEmail().getValue();
        String avatarURL = user.getAvatarURL();

        return new UserModel(email, name, avatarURL);
    }
}
