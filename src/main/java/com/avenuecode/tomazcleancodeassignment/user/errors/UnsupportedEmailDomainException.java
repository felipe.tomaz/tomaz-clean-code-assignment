package com.avenuecode.tomazcleancodeassignment.user.errors;

import lombok.Getter;

public class UnsupportedEmailDomainException extends RuntimeException {
    @Getter private String reason;

    public UnsupportedEmailDomainException(String email) {
        String domain = email.split("@")[1];
        this.reason = String.format("Sorry, domain @%s is not supported yet.", domain);
    }
}
