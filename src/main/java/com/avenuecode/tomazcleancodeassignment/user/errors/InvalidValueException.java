package com.avenuecode.tomazcleancodeassignment.user.errors;

public class InvalidValueException extends RuntimeException {
    public InvalidValueException(String message) {
        super(message);
    }
}
