package com.avenuecode.tomazcleancodeassignment.user.errors;

public class AuthenticationException extends RuntimeException {
    public AuthenticationException() {
        super("Wrong e-mail/password.");
    }
}
