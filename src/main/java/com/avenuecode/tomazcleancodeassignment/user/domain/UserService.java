package com.avenuecode.tomazcleancodeassignment.user.domain;

import com.avenuecode.tomazcleancodeassignment.user.application.dtos.AddUserRequest;
import com.avenuecode.tomazcleancodeassignment.user.application.interfaces.AuthenticationStrategy;
import com.avenuecode.tomazcleancodeassignment.user.application.interfaces.AuthenticationStrategy.UserData;
import com.avenuecode.tomazcleancodeassignment.user.domain.valueobjects.Email;
import com.avenuecode.tomazcleancodeassignment.user.domain.valueobjects.Name;
import com.avenuecode.tomazcleancodeassignment.user.infra.database.UserModel;
import com.avenuecode.tomazcleancodeassignment.user.infra.database.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class UserService {
    @Autowired
    private UserRepository repository;

    public String registerUser(AddUserRequest request, AuthenticationStrategy authService) {
        Email email = new Email(request.getEmail());
        Name name = new Name(request.getName());
        UserData data = authService.authenticate(email, request.getPassword());

        User user = new User(name, email, data.getAvatar());
        repository.save(UserModel.parseToModel(user));

        return data.getToken();
    }
}
