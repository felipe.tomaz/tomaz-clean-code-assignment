package com.avenuecode.tomazcleancodeassignment.user.domain.valueobjects;

import com.avenuecode.tomazcleancodeassignment.user.errors.InvalidValueException;
import lombok.Getter;

import java.util.regex.Pattern;

public class Email {
    @Getter private String value;

    public Email (String email) {
        if(!isValid(email)) {
            throw new InvalidValueException(String.format("%s is not a valid e-mail.", email));
        }

        this.value = email;
    }

    private boolean isValid (String email) {
        Pattern emailPattern = Pattern.compile("^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*$", Pattern.CASE_INSENSITIVE);
        Pattern doubleDotPattern = Pattern.compile("\\.(\\.)+");
        boolean hasDoubleDot = doubleDotPattern.matcher(email).matches();
        boolean isLocalPartWithinRangeLimit = email.split("@")[0].length() < 64;
        boolean hasEmailFormat = emailPattern.matcher(email).matches();

        return !hasDoubleDot && isLocalPartWithinRangeLimit && hasEmailFormat;
    }
}
