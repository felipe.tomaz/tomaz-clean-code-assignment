package com.avenuecode.tomazcleancodeassignment.user.domain.valueobjects;

import com.avenuecode.tomazcleancodeassignment.user.errors.InvalidValueException;
import lombok.Getter;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class Name {
    @Getter private String value;

    public Name (String name) {
        if(name.isBlank()) {
            throw new InvalidValueException("A name cannot be empty.");
        }

        if(name.matches("[0-9]+")) {
            throw new InvalidValueException("A name cannot contains numbers.");
        }

        this.value = format(name);
    }

    private String format(String name) {
        String [] parts = name.split("\\s+");
        List<String> capitalized= Arrays.stream(parts)
                .map(this::capitalize)
                .collect(Collectors.toList());

        return String.join(" ", capitalized);
    }

    private String capitalize(String value) {
        String firstLetter = value.substring(0, 1).toUpperCase();
        String rest = value.substring(1).toLowerCase();

        return firstLetter.concat(rest);
    }
}
