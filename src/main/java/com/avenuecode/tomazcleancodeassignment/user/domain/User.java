package com.avenuecode.tomazcleancodeassignment.user.domain;

import com.avenuecode.tomazcleancodeassignment.user.domain.valueobjects.Email;
import com.avenuecode.tomazcleancodeassignment.user.domain.valueobjects.Name;
import lombok.Data;


@Data
public class User {
     private Email email;
     private Name name;
     private String avatarURL;

    public User(Name name, Email email, String avatarURL) {
        this.email = email;
        this.name = name;
        this.avatarURL = avatarURL;
    }
}
