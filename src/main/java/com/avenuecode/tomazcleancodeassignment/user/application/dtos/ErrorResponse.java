package com.avenuecode.tomazcleancodeassignment.user.application.dtos;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class ErrorResponse {
    private String reason;
}
