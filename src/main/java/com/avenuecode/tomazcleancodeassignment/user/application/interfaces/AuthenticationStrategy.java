package com.avenuecode.tomazcleancodeassignment.user.application.interfaces;

import com.avenuecode.tomazcleancodeassignment.user.domain.valueobjects.Email;
import com.avenuecode.tomazcleancodeassignment.user.errors.AuthenticationException;
import lombok.AllArgsConstructor;
import lombok.Data;

public interface AuthenticationStrategy {
    UserData authenticate(Email email, String password) throws AuthenticationException;

    @Data
    @AllArgsConstructor
    class UserData {
        private String avatar;
        private String token;
    }
}


