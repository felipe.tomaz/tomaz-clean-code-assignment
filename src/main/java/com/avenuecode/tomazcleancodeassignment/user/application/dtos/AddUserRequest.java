package com.avenuecode.tomazcleancodeassignment.user.application.dtos;

import lombok.Data;

import javax.validation.constraints.NotNull;

@Data
public class AddUserRequest {
    @NotNull private String name;
    @NotNull private String email;
    @NotNull private String password;
}
