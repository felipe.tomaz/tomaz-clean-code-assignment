package com.avenuecode.tomazcleancodeassignment.user.application;

import com.avenuecode.tomazcleancodeassignment.user.application.dtos.AddUserRequest;
import com.avenuecode.tomazcleancodeassignment.user.application.dtos.AddUserResponse;
import com.avenuecode.tomazcleancodeassignment.user.application.interfaces.AuthenticationStrategy;
import com.avenuecode.tomazcleancodeassignment.user.domain.UserService;
import com.avenuecode.tomazcleancodeassignment.user.infra.authentication.AuthenticatorFactory;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@Controller
@RequestMapping("/users")
public class UserController {

    private UserService userService;

    public UserController(UserService userService) {
        this.userService = userService;
    }

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    @ResponseBody
    public AddUserResponse createUser(@Valid @RequestBody AddUserRequest request) {
        AuthenticationStrategy authenticator = AuthenticatorFactory.createAuthenticator(request.getEmail());

        String token = this.userService.registerUser(request, authenticator);

        return new AddUserResponse(token);
    }
}
