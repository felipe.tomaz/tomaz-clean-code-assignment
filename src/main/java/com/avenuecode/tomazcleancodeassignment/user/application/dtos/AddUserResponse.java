package com.avenuecode.tomazcleancodeassignment.user.application.dtos;

import lombok.AllArgsConstructor;
import lombok.Data;


@AllArgsConstructor
@Data
public class AddUserResponse {
    private String token;
}