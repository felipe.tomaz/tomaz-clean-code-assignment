package com.avenuecode.tomazcleancodeassignment.user.application;

import com.avenuecode.tomazcleancodeassignment.user.application.dtos.ErrorResponse;
import com.avenuecode.tomazcleancodeassignment.user.errors.InvalidValueException;
import com.avenuecode.tomazcleancodeassignment.user.errors.UnsupportedEmailDomainException;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

import javax.servlet.http.HttpServletRequest;

@ControllerAdvice
public class UserExceptionHandler {
    @ExceptionHandler(InvalidValueException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ResponseBody
    public ErrorResponse handleInvalidValueException(HttpServletRequest req, InvalidValueException ex) {
        return new ErrorResponse(ex.getMessage());
    }

    @ExceptionHandler(UnsupportedEmailDomainException.class)
    @ResponseStatus(HttpStatus.NOT_ACCEPTABLE)
    @ResponseBody
    public ErrorResponse handleInvalidDomainException(HttpServletRequest req, UnsupportedEmailDomainException ex) {
        return new ErrorResponse(ex.getReason());
    }
}
